import mdp, util
from learningAgents import ValueEstimationAgent

# ===========================================================================================
# valueIterationAgents.py
# -------------------------------------------------------------------------------------------
# Licensing Information:  You are free to use or extend these projects for 
# educational purposes provided that (1) you do not distribute or publish 
# solutions, (2) you retain this notice, and (3) you provide clear 
# attribution to UC Berkeley, including a link to 
# http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero 
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and 
# Pieter Abbeel (pabbeel@cs.berkeley.edu).
# -------------------------------------------------------------------------------------------
# Angela Gross
# CSCI 555
# Reinforcement Learning
# ===========================================================================================

# ////////////////////////////////////////////////////////////////////////////////////////////////


"""
    * You should read learningAgents.py before reading this.*

    A ValueIterationAgent takes a Markov decision process
    (see mdp.py) on initialization and runs value iteration
    for a given number of iterations using the supplied
    discount factor.

"""
class ValueIterationAgent(ValueEstimationAgent):

    """
      Your value iteration agent should take an mdp on
      construction, run the indicated number of iterations
      and then act according to the resulting policy.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 100):

        # Initialize agent
        self.mdp = mdp
        self.discount = discount
        self.iterations = iterations
        self.values = util.Counter() # Current values (defaults to 0)
        self.prevValues = util.Counter() # Last iteration's values (defaults to 0)
        self.policies = {} # Chosen actions for each state

        # Value iteration
        self.iterateValues()

    """
        V(s) = max_{a in actions} Q(s,a)
    """
    def iterateValues(self):

        # Get states and gamma
        states = self.mdp.getStates()
        gamma = self.discount

        while self.iterations > 0:

            # Iterate over states
            for state in states:

                # If it isn't a terminal state, then grab the possible actions and find the best QVal
                if not self.mdp.isTerminal(state):

                    # Get actions
                    actions = self.mdp.getPossibleActions(state)
                    QValues = util.Counter()

                    # Iterate over actions
                    for action in actions:

                        stateProbs = self.mdp.getTransitionStatesAndProbs(state, action)

                        # Iterate over transitions
                        for (newState, prob) in stateProbs:
                            # Get reward and add to QValue for a given action
                            reward = self.mdp.getReward(state, action, newState)
                            QValues[action] += prob * ( reward + ( gamma * self.prevValues[newState] ) )

                    # Checking if there were any items before computing
                    if len(QValues.items()) > 0:
                        # Compute max and update value and policy for state-action pair
                        maxAction = QValues.argMax()
                        self.values[state] = QValues[maxAction]
                        self.policies[state] = maxAction

            # Update iterations done and keep track of last iteration
            self.iterations = self.iterations - 1
            self.prevValues = self.values.copy()

    """
      Compute the Q-value of action in state from the value
      function stored in self.values, or in one that is passed in.
    """
    def computeQValueFromValues(self, state, action):

        stateProbs = self.mdp.getTransitionStatesAndProbs(state, action)
        gamma = self.discount
        QValue = 0.0

        # Iterate over transitions
        for (newState, prob) in stateProbs:

            # Get reward
            reward = self.mdp.getReward(state, action, newState)

            # Add to value
            QValue += prob * ( reward + ( gamma * self.values[newState] ) )

        return QValue

    """
      The policy is the best action in the given state according
      to the values currently stored in self.values.

      You may break ties any way you see fit.  Note that if there
      are no legal actions, which is the case at the terminal
      state, you should return None.

      policy(s) = arg_max_{a in actions} Q(s,a)

      If it doesn't exist in the dictionary, then the "get"
      function will return 'None' (so it fits perfect for edge
      cases)
    """
    def computeActionFromValues(self, state):
        return self.policies.get(state)

    """
      Return the value of the state (computed in __init__).
    """
    def getValue(self, state):
        return self.values[state]

    def getPolicy(self, state):
        return self.computeActionFromValues(state)

    "Returns the policy at the state (no exploration)."
    def getAction(self, state):
        return self.computeActionFromValues(state)

    def getQValue(self, state, action):
        return self.computeQValueFromValues(state, action)

# ////////////////////////////////////////////////////////////////////////////////////////////////
