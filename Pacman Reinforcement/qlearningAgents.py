from game import *
from learningAgents import ReinforcementAgent
from featureExtractors import *

import random,util,math

# ===========================================================================================
# qlearningAgents.py
# -------------------------------------------------------------------------------------------
# Licensing Information:  You are free to use or extend these projects for 
# educational purposes provided that (1) you do not distribute or publish 
# solutions, (2) you retain this notice, and (3) you provide clear 
# attribution to UC Berkeley, including a link to 
# http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero 
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and 
# Pieter Abbeel (pabbeel@cs.berkeley.edu).
# -------------------------------------------------------------------------------------------
# Angela Gross
# CSCI 555
# Reinforcement Learning
# ===========================================================================================

# ////////////////////////////////////////////////////////////////////////////////////////////////

"""
  Q-Learning Agent

  Functions you should fill in:
    - computeValueFromQValues
    - computeActionFromQValues
    - getQValue
    - getAction
    - update

  Instance variables you have access to
    - self.epsilon (exploration prob)
    - self.alpha (learning rate)
    - self.discount (discount rate)

  Functions you should use
    - self.getLegalActions(state) which returns legal actions for a state
"""
class QLearningAgent(ReinforcementAgent):

    def __init__(self, **args):
        "You can initialize Q-values here..."
        ReinforcementAgent.__init__(self, **args)
        self.QValues = util.Counter() # Current values (defaults to 0)

    """
      Returns Q(state, action)
      Returns 0.0 if we have never seen a state
      or the Q node value otherwise
    """
    def getQValue(self, state, action):
        return self.QValues[ (state, action) ]

    """
      Returns max_action Q(state,action)
      Where the max is over legal actions.  Note that if
      there are no legal actions, which is the case at the
      terminal state, you should return a value of 0.0.
    """
    def computeValueFromQValues(self, state):

        QValue = 0.0

        # Get actions
        legalActions = self.getLegalActions(state)

        # If non-empty, then find maximum QValue over legal actions
        if legalActions:
            QValue = max(self.getQValue(state, action) for action in legalActions)

        return QValue

    """
      Compute the best action to take in a state.  Note that if there
      are no legal actions, which is the case at the terminal state,
      you should return None.
    """
    def computeActionFromQValues(self, state):

        bestAction = None

        # Get actions
        legalActions = self.getLegalActions(state)

        # If non-empty, then find action with best QVal
        if legalActions:
            # Get value action pairs
            actionValuePairs = { action:self.getQValue(state, action) for action in legalActions }
            # Separate keys (actions)
            keys = list(actionValuePairs.keys())
            # Separate values (QValues)
            values = list(actionValuePairs.values())
            # Find max QValue
            maxQValue = max(values)
            # Find action for that max
            bestAction = keys[values.index(maxQValue)]

        return bestAction

    """
      Compute the action to take in the current state.  With
      probability self.epsilon, we should take a random action and
      take the best policy action otherwise.  Note that if there are
      no legal actions, which is the case at the terminal state, you
      should choose None as the action.

      HINT: You might want to use util.flipCoin(prob)
      HINT: To pick randomly from a list, use random.choice(list)
    """
    def getAction(self, state):

        action = None

        # Get actions
        legalActions = self.getLegalActions(state)

        # Figure out if it'll be a random action (exploration) or
        # best action (exploitation)
        randomAction = util.flipCoin(self.epsilon)

        # Get action based on coin flip
        if randomAction:
            action = random.choice(legalActions)
        else:
            action = self.getPolicy(state)

        return action

    """
      The parent class calls this to observe a
      state = action => nextState and reward transition.
      You should do your Q-Value update here.

      NOTE: You should never call this function,
      it will be called on your behalf.
    """
    def update(self, state, action, nextState, reward):

        # Compute sample = R(s, a, s') + lambda * max_a' Q(s', a')
        sample = reward + self.discount * self.getValue(nextState)

        # Compute running average of QValue = (1 - a)Q(s,a) + (a)(sample)
        newQValue = (1 - self.alpha) * self.getQValue(state, action) + self.alpha * sample

        # Update QValue
        self.QValues[(state, action)] = newQValue

    def getPolicy(self, state):
        return self.computeActionFromQValues(state)

    def getValue(self, state):
        return self.computeValueFromQValues(state)

# ////////////////////////////////////////////////////////////////////////////////////////////////

"Exactly the same as QLearningAgent, but with different default parameters."
class PacmanQAgent(QLearningAgent):

    """
        These default parameters can be changed from the pacman.py command line.
        For example, to change the exploration rate, try:
            python pacman.py -p PacmanQLearningAgent -a epsilon=0.1

        alpha    - learning rate
        epsilon  - exploration rate
        gamma    - discount factor
        numTraining - number of training episodes, i.e. no learning after these many episodes
    """
    def __init__(self, epsilon=0.05,gamma=0.8,alpha=0.2, numTraining=0, **args):
        args['epsilon'] = epsilon
        args['gamma'] = gamma
        args['alpha'] = alpha
        args['numTraining'] = numTraining
        self.index = 0  # This is always Pacman
        QLearningAgent.__init__(self, **args)

    """
        Simply calls the getAction method of QLearningAgent and then
        informs parent of action for Pacman.  Do not change or remove this
        method.
    """
    def getAction(self, state):

        action = QLearningAgent.getAction(self,state)
        self.doAction(state,action)
        return action

# ////////////////////////////////////////////////////////////////////////////////////////////////

"""
   ApproximateQLearningAgent

   You should only have to overwrite getQValue
   and update.  All other QLearningAgent functions
   should work as is.
"""
class ApproximateQAgent(PacmanQAgent):

    def __init__(self, extractor='IdentityExtractor', **args):
        self.featExtractor = util.lookup(extractor, globals())()
        PacmanQAgent.__init__(self, **args)
        self.weights = util.Counter()

    def getWeights(self):
        return self.weights

    """
      Should return Q(state,action) = w * featureVector
      where * is the dotProduct operator
    """
    def getQValue(self, state, action):

        QValue = 0.0

        # Get features
        features = self.featExtractor.getFeatures(state, action)

        # Add weight and feature dot products
        for feature in features.keys():
            QValue += self.weights[feature] * features[feature]

        return QValue

    """
       Should update your weights based on transition
    """
    def update(self, state, action, nextState, reward):

        weight = 0.0

        # Get features
        features = self.featExtractor.getFeatures(state, action)

        # Get difference = ( R(s, a, s') + lambda * max_a' Q(s', a') ) - Q(s, a)
        difference = ( reward + self.discount * self.getValue(nextState) ) - self.getQValue(state, action)

        # Compute running average for weight and update weight
        for feature in features.keys():
            newWeight = self.weights[feature] + self.alpha * difference * features[feature]
            self.weights[feature] = newWeight

    "Called at the end of each game."
    def final(self, state):

        # call the super-class final method
        PacmanQAgent.final(self, state)

        # did we finish training?
        if self.episodesSoFar == self.numTraining:
            # you might want to print your weights here for debugging
            pass

# ////////////////////////////////////////////////////////////////////////////////////////////////