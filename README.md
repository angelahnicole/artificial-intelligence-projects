# Artificial Intelligence Pacman Projects: Overview

This contains the Pacman projects that I worked on in my Artificial Intelligence class, CSCI 555, at the University of Montana. These projects were derived from the Berkeley Artificial Intelligence class, which you can find here: http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html.

Note that the majority of the source files were pre-created by the instructors, but I will note which files were modified by me for each project. These files are well commented and explain what is being done.

# How to run the game

It's a little different for each project, but you basically run the python.py file like so:

```
#!bash

python pacman.py
```

And use the keys 'a', 's', 'd', and 'w' to move (or arrow keys) when you're playing.

You can always pass in `--help` for more information, i.e.

```
#!bash

python pacman.py --help
```

If you would like to grade the projects yourself, then you can use the autograder:

```
#!bash

python autograder.py
```

# Pacman Ghostbusters

## Relevant/Changed Files:
```
- inference.py
- bustersAgents.py
```

Note that, if you run the autograder on this, questions 4 - 7 will fail because they were not assigned.

# Pacman Heuristic Search

## Relevant/Changed Files:
```
- inference.py
- bustersAgents.py
```

# Pacman Multiagent

## Relevant/Changed Files:
```
- multiAgents.py
```

# Pacman Reinforcement

## Relevant/Changed Files:
```
- qlearningAgents.py
- analysis.py
```