from util import manhattanDistance
from game import Directions
import random, util
from game import Agent

# ===========================================================================================
# multiAgents.py
# -------------------------------------------------------------------------------------------
# Licensing Information:  You are free to use or extend these projects for 
# educational purposes provided that (1) you do not distribute or publish 
# solutions, (2) you retain this notice, and (3) you provide clear 
# attribution to UC Berkeley, including a link to 
# http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero 
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and 
# Pieter Abbeel (pabbeel@cs.berkeley.edu).
# -------------------------------------------------------------------------------------------
# Angela Gross
# CSCI 555
# Multiagent
# ===========================================================================================

# ////////////////////////////////////////////////////////////////////////////////////////////////

"""
  A reflex agent chooses an action at each choice point by examining
  its alternatives via a state evaluation function.

  The evaluation function will return a higher score if food is close and will return a lower score
  if ghosts are close.
"""
class ReflexAgent(Agent):

    """
    getAction chooses among the best options according to the evaluation function.
    """
    def getAction(self, gameState):

        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        return legalMoves[chosenIndex]

    """
    This will add to the score if food is close and will subtract from the score
    if ghosts are close. It will add the score if ghosts are close and they are
    scared, however. Also penalizes for stopping.
    """
    def evaluationFunction(self, currentGameState, action):

        # Successor State Information
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPosition = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newCapsules = successorGameState.getCapsules()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]
        newGhostPositions = successorGameState.getGhostPositions()

        # Current State Information
        currentPosition = currentGameState.getPacmanPosition()

        # Base score and scared estimate
        score = successorGameState.getScore()
        scaredTimeEstimate = newScaredTimes[0]
        ghostsScared = self.areGhostsScared(scaredTimeEstimate)

        # Get scores based on manhattan distances of food, ghosts, and capsules
        ghostScore = self.getGhostScore(newPosition, newGhostPositions)
        foodScore = self.getFoodScore(newPosition, newFood)
        capsuleScore = self.getCapsuleScore(newPosition, newCapsules)

        # If the ghost is within two units (since this is the successor), we want to run!
        if not ghostsScared and ghostScore <= 1:
            score = score - float('inf')
        # If the ghost is within two units (since this is the successor), we want to pursue!
        elif ghostsScared and ghostScore <= 1:
            score = score + float('inf')
        # If all is clear, then get some food! (but favor close capsules)
        else:
            score = score + foodScore
            if capsuleScore <= 1: score = score + 100

        # Penalize stopping
        if currentPosition == newPosition: score = score - 5

        return score

    """
    Using how many moves are left in "scared mode" from the first ghost, return true or false
    """
    def areGhostsScared(self, scaredTimeEstimate):
      if scaredTimeEstimate > 1: return True
      else: return False

    """
    Returns a score that is the cumulative addition of the ghost's manhattan distance from pacman.
    """
    def getGhostScore(self, currentPosition, ghostPositions):

      # Default score
      score = 0.0

      # Add together the manhattan distances of the ghosts
      for myGhost in ghostPositions:
            score = score + manhattanDistance(currentPosition, myGhost)

      return score

    """
    Returns a score that is the cumulative addition of the reciprocal of the food distance from pacman.
    """
    def getFoodScore(self, currentPosition, food):

      # Default score
      score = 0.0

      # Convert food to list
      foodList = food.asList()

      # Add together the reciprocal of food distance (multiplied by 10)
      for myFood in foodList:
          score = score + ( 1/float( manhattanDistance(currentPosition, myFood) + 1 ) ) * 10

      return score

    """
    Returns a score that is the cumulative addition of the capsule distance from pacman.
    """
    def getCapsuleScore(self, currentPosition, capsules):

        # Default score
        score = 0.0

        # Add together the reciprocal of food distance (multiplied by 10)
        for capsule in capsules:
            score = score + manhattanDistance(currentPosition, capsule)

        return score

# ////////////////////////////////////////////////////////////////////////////////////////////////

"""
This default evaluation function just returns the score of the state.
The score is the same one displayed in the Pacman GUI.

This evaluation function is meant for use with adversarial search agents
(not reflex agents).
"""
def scoreEvaluationFunction(currentGameState):

    return currentGameState.getScore()

# ////////////////////////////////////////////////////////////////////////////////////////////////

"""
  This class provides some common elements to all of your
  multi-agent searchers.  Any methods defined here will be available
  to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

  You *do not* need to make any changes here, but you can if you want to
  add functionality to all your adversarial search agents.  Please do not
  remove anything, however.

  Note: this is an abstract class: one that should not be instantiated.  It's
  only partially specified, and designed to be extended.  Agent (game.py)
  is another abstract class.
"""
class MultiAgentSearchAgent(Agent):

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

# ////////////////////////////////////////////////////////////////////////////////////////////////

"""
  Your minimax agent (question 2)
"""
class MinimaxAgent(MultiAgentSearchAgent):

    """
    Returns the minimax action from the current gameState using self.depth
    and self.evaluationFunction.

    Here are some method calls that might be useful when implementing minimax.

    gameState.getLegalActions(agentIndex):
      Returns a list of legal actions for an agent
      agentIndex=0 means Pacman, ghosts are >= 1

    gameState.generateSuccessor(agentIndex, action):
      Returns the successor game state after an agent takes an action

    gameState.getNumAgents():
      Returns the total number of agents in the game
    """
    def getAction(self, gameState):

        # Set default value
        bestAction = Directions.STOP

        # Accumulate values, starting with Pacman and the first ghost
        agentMinValues = { action : self.minimax(self.depth, 1, gameState.generateSuccessor(0, action)) for action in gameState.getLegalActions(0) }

        # Find largest value (and action) from the ones found
        if len(agentMinValues) > 0:
            bestValue = max( agentMinValues.values() )
            bestActions = [ action for action,value in agentMinValues.iteritems() if value == bestValue ]
            bestAction = bestActions[0]

        return bestAction

    """
    Basic minimax but with multiple minimizing agents (i.e. ghosts), with the "nodes" as the successor game state.
    """
    def minimax(self, depth, agentIndex, state):

        # Base Case
        if depth == 0 or state.isWin() or state.isLose():
            return self.evaluationFunction(state)

        # Pacman (maximizing case)
        if agentIndex == 0:

            # Set default, worst-case value
            bestValue = float('-inf')

            # Get all values ( don't change depth until after all players have gone)
            agentMinValues = [ self.minimax(depth, agentIndex + 1, state.generateSuccessor(agentIndex, action)) for action in state.getLegalActions(agentIndex) ]

            # Find largest value
            if len(agentMinValues) > 0: bestValue = max(agentMinValues)

            return bestValue

        # Ghost (minimizing case)
        else:

            # Set default, worst-case value
            bestValue = float('inf')

            # Last ghost, need to examine max node (i.e. Pacman)
            if agentIndex == state.getNumAgents() - 1:

                # Get all values ( don't change depth until after all players have gone, but in this case they all have gone )
                agentMaxValues = [ self.minimax(depth - 1, 0, state.generateSuccessor(agentIndex, action)) for action in state.getLegalActions(agentIndex) ]
                # Find smallest value
                if len(agentMaxValues) > 0: bestValue = min(agentMaxValues)

            # Still examining all of the other ghosts, so still examining min nodes
            else:

                # Get all values ( don't change depth until after all players have gone )
                agentMinValues = [ self.minimax(depth, agentIndex + 1, state.generateSuccessor(agentIndex, action)) for action in state.getLegalActions(agentIndex) ]
                # Find smallest value
                if len(agentMinValues) > 0: bestValue = min(agentMinValues)

            return bestValue

# ////////////////////////////////////////////////////////////////////////////////////////////////

"""
  Your minimax agent with alpha-beta pruning (question 3)
"""
class AlphaBetaAgent(MultiAgentSearchAgent):

    """
      Returns the minimax action using self.depth and self.evaluationFunction
    """
    def getAction(self, gameState):

        # Set default values for alpha and beta
        alpha = float('-inf')
        beta = float('inf')

        # Set default values of best value-action pair
        bestValue = float('-inf')
        bestAction = Directions.STOP

        # Accumulate value-action pairs, starting with Pacman and the first ghost
        for action in gameState.getLegalActions(0):
            # Update alpha and beta and get best value from children
            v = self. alphabeta(self.depth, 1, gameState.generateSuccessor(0, action), alpha, beta)
            # Update the max value / action pair seen thus far
            if v > bestValue:
                bestValue = v
                bestAction = action
            # Check if we need to prune
            if bestValue > beta: break
            # Update alpha
            alpha = max(alpha, bestValue)

        return bestAction

    """
    Alphabeta but with multiple minimizing agents (i.e. ghosts) and one maximizing agent (i.e. Pacman) with the "nodes"
    as the successor game state.
    """
    def alphabeta(self, depth, agentIndex, state, alpha, beta):

        # Base Case
        if depth == 0 or state.isWin() or state.isLose():
            return self.evaluationFunction(state)

        # Pacman (maximizing case)
        if agentIndex == 0:

            # Set default, worst-case value
            bestValue = float('-inf')

            # Compare successor values ( don't change depth until after all players have gone)
            for action in state.getLegalActions(agentIndex):
                # Find maximum between what we have and what children have (first member of returned tuple is value)
                bestValue = max(bestValue, self.alphabeta(depth, agentIndex + 1, state.generateSuccessor(agentIndex, action), alpha, beta ) )
                # Prune if "a waste of time"
                if bestValue > beta: return bestValue
                # Update alpha
                alpha = max(alpha, bestValue)

            return bestValue

        # Ghost (minimizing case)
        else:

            # Set default, worst-case value
            bestValue = float('inf')

            # Last ghost, need to examine max node (i.e. Pacman)
            if agentIndex == state.getNumAgents() - 1:

                # Compare successor values ( don't change depth until after all players have gone, and in this case they all have ))
                for action in state.getLegalActions(agentIndex):
                    # Find minimum between what we have and what children have (first member of returned tuple is value)
                    bestValue = min(bestValue, self.alphabeta(depth - 1, 0, state.generateSuccessor(agentIndex, action), alpha, beta ) )
                    # Prune if "a waste of time"
                    if bestValue < alpha: return bestValue
                    # Update beta
                    beta = min(beta, bestValue)

            # Still examining all of the other ghosts, so still examining min nodes
            else:

                # Compare successor values ( don't change depth until after all players have gone)
                for action in state.getLegalActions(agentIndex):
                    # Find minimum between what we have and what children have (first member of returned tuple is value)
                    bestValue = min(bestValue, self.alphabeta(depth, agentIndex + 1, state.generateSuccessor(agentIndex, action), alpha, beta ) )
                    # Prune if "a waste of time"
                    if bestValue < alpha: return bestValue
                    # Update beta
                    beta = min(beta, bestValue)

            return bestValue

# ////////////////////////////////////////////////////////////////////////////////////////////////

"""
  Your expectimax agent (question 4)
"""
class ExpectimaxAgent(MultiAgentSearchAgent):

    """
      Returns the expectimax action using self.depth and self.evaluationFunction

      All ghosts should be modeled as choosing uniformly at random from their legal moves.
    """
    def getAction(self, gameState):

        # Set default value
        bestAction = Directions.STOP

        # Accumulate values, starting with Pacman and the first ghost
        agentMinValues = { action : self.expectimax(self.depth, 1, gameState.generateSuccessor(0, action)) for action in gameState.getLegalActions(0) }

        # Find largest value (and action) from the ones found
        if len(agentMinValues) > 0:
            bestValue = max( agentMinValues.values() )
            bestActions = [ action for action,value in agentMinValues.iteritems() if value == bestValue ]
            bestAction = bestActions[0]

        return bestAction

    """
    Expectimax but with multiple random agents (i.e. ghosts) and one maximizing agent (i.e. Pacman) with the "nodes"
    as the successor game state.
    """
    def expectimax(self, depth, agentIndex, state):

        # Base Case
        if depth == 0 or state.isWin() or state.isLose():
            return self.evaluationFunction(state)

        # Pacman (maximizing case)
        if agentIndex == 0:

            # Set default, worst-case value
            bestValue = float('-inf')

            # Get all values ( don't change depth until after all players have gone)
            agentAvgValues = [ self.expectimax(depth, agentIndex + 1, state.generateSuccessor(agentIndex, action)) for action in state.getLegalActions(agentIndex) ]

            # Find largest value
            if len(agentAvgValues) > 0: bestValue = max(agentAvgValues)

            return bestValue

        # Ghost (random case)
        else:

            # Set default value
            bestValue = 0

            # Last ghost, need to examine max node (i.e. Pacman)
            if agentIndex == state.getNumAgents() - 1:

                # Get all values ( don't change depth until after all players have gone, but in this case they all have gone )
                agentMaxValues = [ self.expectimax(depth - 1, 0, state.generateSuccessor(agentIndex, action)) for action in state.getLegalActions(agentIndex) ]
                # Find average (child nodes are equally weighted)
                if len(agentMaxValues) > 0: bestValue = sum(agentMaxValues) / len(agentMaxValues)

            # Still examining all of the other ghosts, so still examining random nodes
            else:

                # Get all values ( don't change depth until after all players have gone )
                agentAvgValues = [ self.expectimax(depth, agentIndex + 1, state.generateSuccessor(agentIndex, action)) for action in state.getLegalActions(agentIndex) ]
                # Find average (child nodes are equally weighted)
                if len(agentAvgValues) > 0: bestValue = sum(agentAvgValues) / len(agentAvgValues)

            return bestValue

# ////////////////////////////////////////////////////////////////////////////////////////////////

# Keep track of positions and times visited
pacPositionValues = {}

"""
  Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
  evaluation function (question 5).

  DESCRIPTION:
  -->   I subtracted 3000 from the score if a ghost is close (i.e. within one unit) and not scared.
  -->   I added 3000 to the score if a ghost is close and is scared (I took the average of the scared times).
  -->   I added the product of the reciprocal of the fod distance and 20 to the score.
  -->   I added the product of the reciprocal of the capsule distance and 1000 if it's close (i.e. within 4 units) to
        the score or add 1200 if we just ate a capsule.
  -->   I subtract the product of how many times we've visited a position by 15 to discourage thrashing. However, this
        is reset every 10 turns so Pacman isn't discouraged to eat pellets.
  -->   I subtract the product of how many pellets are left and 10 so Pacman is encouraged to eat pellets at the end of
        the game.
"""
def betterEvaluationFunction(currentGameState):

    # Current State Information
    currentPosition = currentGameState.getPacmanPosition()
    currentFood = currentGameState.getFood()
    numFood = len(currentFood.asList())
    currentCapsules = currentGameState.getCapsules()
    currentGhostStates = currentGameState.getGhostStates()
    currentScaredTimes = [ghostState.scaredTimer for ghostState in currentGhostStates]
    currentGhostPositions = currentGameState.getGhostPositions()

    # Base score and scared estimate
    score =  currentGameState.getScore()
    scaredTimeEstimate =  sum(currentScaredTimes) / len(currentScaredTimes)
    ghostsScared = areGhostsScared(scaredTimeEstimate)

    # Get scores based on manhattan distances of food, ghosts, and capsules
    ghostScore = getGhostScore(currentPosition, currentGhostPositions)
    foodScore = getFoodScore(currentPosition, currentFood)
    capsuleScore = getCapsuleScore(currentPosition, currentCapsules)

    # If the ghost is within one unit, we want to run!
    if not ghostsScared and ghostScore <= 1:
        score = score - 3000
    # If the ghost is within one unit, we want to pursue!
    elif ghostsScared and ghostScore <= 1:
        score = score + 3000
    # If all is clear, then get some food! (but favor close capsules)
    else:
        score = score + foodScore
        if capsuleScore <= 4 and capsuleScore > 0: score = score + ( 1 / float(capsuleScore) ) * 1000
        if capsuleScore == 0: score = score + 1200

    # Penalize going to the same spot to prevent thrashing
    if currentPosition in pacPositionValues.keys():
        pacPositionValues[currentPosition] += 1
        score = score - pacPositionValues[currentPosition] * 15
    # Add position to places visited
    else:
        pacPositionValues[currentPosition] = 1

    # Clean up positions - helps prevent overriding pellet-eating if only keep track of recent 10
    if len(pacPositionValues) >= 10: pacPositionValues.clear()

    # Make it worth more to grab food as less pellets show up
    score = score - 10 * numFood

    return score

"""
Using how many moves are left in "scared mode" from the first ghost, return true or false
"""
def areGhostsScared(scaredTimeEstimate):
  if scaredTimeEstimate > 1: return True
  else: return False

"""
Returns a score that is the cumulative addition of the ghost's manhattan distance from pacman.
"""
def getGhostScore(currentPosition, ghostPositions):

  # Default score
  score = 0.0

  # Add together the manhattan distances of the ghosts
  for myGhost in ghostPositions:
        score = score + manhattanDistance(currentPosition, myGhost)

  return score

"""
Returns a score that is the cumulative addition of the reciprocal of the food distance from pacman.
"""
def getFoodScore(currentPosition, food):

  # Default score
  score = 0.0

  # Convert food to list
  foodList = food.asList()

  # Add together the reciprocal of food distance (multiplied by 20)
  for myFood in foodList:
      score = score + ( 1/float( manhattanDistance(currentPosition, myFood) + 1 ) ) * 20

  return score

"""
Returns a score that is the cumulative addition of the capsule distance from pacman.
"""
def getCapsuleScore(currentPosition, capsules):

    # Default score
    score = 0.0

    # Add together the distance of capsules
    for capsule in capsules:
        score = score + manhattanDistance(currentPosition, capsule)

    return score

# ////////////////////////////////////////////////////////////////////////////////////////////////

# Abbreviation
better = betterEvaluationFunction

# ////////////////////////////////////////////////////////////////////////////////////////////////

