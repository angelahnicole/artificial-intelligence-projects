
import util

# //////////////////////////////////////////////////////////////////////////////
# search.py
# ------------------------------------------------------------------------------
# Licensing Information:  You are free to use or extend these projects for 
# educational purposes provided that (1) you do not distribute or publish 
# solutions, (2) you retain this notice, and (3) you provide clear 
# attribution to UC Berkeley, including a link to 
# http://inst.eecs.berkeley.edu/~cs188/pacman/pacman.html
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero 
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and 
# Pieter Abbeel (pabbeel@cs.berkeley.edu).
# ------------------------------------------------------------------------------
# AI: CSCI 446
# Blind Search
# Angela Gross
# //////////////////////////////////////////////////////////////////////////////

"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""
# //////////////////////////////////////////////////////////////////////////////

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You should not change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()
    
    def isGoalState(self, state):
        """
            state: Search state
    
        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()
    
    def getSuccessors(self, state):
        """
            state: Search state
    
        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()
    
    def getCostOfActions(self, actions):
        """
            actions: A list of actions to take
    
        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()

# //////////////////////////////////////////////////////////////////////////////
# //////////////////////////////////////////////////////////////////////////////

"""
Returns a sequence of moves that solves tinyMaze.  For any other maze, the
sequence of moves will be incorrect, so only use this for tinyMaze.
"""
def tinyMazeSearch(problem):

    from game import Directions
    s = Directions.SOUTH
    # The value of s is 'South'
    n = Directions.NORTH
    e = Directions.EAST
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

# //////////////////////////////////////////////////////////////////////////////

"""Search the deepest nodes in the search tree first."""
def depthFirstSearch(problem):
   
    # Set up stack of successor nodes and set of discovered nodes
    myStack = util.Stack()
    myDiscoveredSet = set()
    
    # Get starting position and push it on stack
    startPosition = problem.getStartState()
    startNode = (startPosition, None, 1)
    myStack.push( (startNode, []) )
    
    # Move through stack of successors
    while not myStack.isEmpty():
        
        # Get node, path, and position
        (myNode, myPath) = myStack.pop()       
        myPosition = myNode[0]
        
        # Check if position in goal state
        if problem.isGoalState(myPosition):
            return myPath
        
        # Check if not seen already
        if myPosition not in myDiscoveredSet:

            # Add to set of discovered positions
            myDiscoveredSet = myDiscoveredSet.union( set([myPosition]) )
            
            for successorNode in problem.getSuccessors(myPosition):
                # Get full path
                successorFullPath = myPath + [successorNode[1]]
                # Push successor onto stack
                myStack.push( (successorNode, successorFullPath) )
    
    # No path found         
    return None

# //////////////////////////////////////////////////////////////////////////////  

"""Search the shallowest nodes in the search tree first."""
def breadthFirstSearch(problem):
    
    # Set up Q of nodes and set of discovered nodes
    myQ = util.Queue()
    myDiscoveredSet = set()

    # Get starting position, push it back, and add to set of discovered nodes
    startPosition = problem.getStartState()
    startNode = (startPosition, None, 1)
    myQ.push( (startNode, []) )
    myDiscoveredSet = myDiscoveredSet.union( set([startPosition]) )

    # Move through Q of successors
    while not myQ.isEmpty():

        # Get node, path, and position
        (myNode, myPath) = myQ.pop()       
        myPosition = myNode[0]

        # Check if position in goal state
        if problem.isGoalState(myPosition):
            return myPath

        # Loop through successors
        for successorNode in problem.getSuccessors(myPosition):

            # Get position and full path
            successorPosition = successorNode[0]
            successorFullPath = myPath + [successorNode[1]]

            # Check if seen already
            if successorPosition not in myDiscoveredSet:
                # Add to set of discovered positions
                myDiscoveredSet = myDiscoveredSet.union( set([successorPosition]) )
                # Push onto Q ("enqueue")
                myQ.push( (successorNode, successorFullPath) )

    # No path found 
    return None

# //////////////////////////////////////////////////////////////////////////////

"""Search the node of least total cost first."""
def uniformCostSearch(problem):
    
    # Set up a priority Q, set of discovered nodes, and dictionary of costs
    myPriorityQ = util.PriorityQueue()
    myDiscoveredSet = set()
    myCostDict = {}
 
    # Get starting position, push it on priority Q (with cost 0), discovered, and add cost
    startPosition = problem.getStartState()
    startPath = []
    startNode = (startPosition, None, 1)
    myPriorityQ.push( (startNode, startPath), 0 )
    myDiscoveredSet = myDiscoveredSet.union( set([startPosition]) )
    myCostDict[startPosition] = 0

    while not myPriorityQ.isEmpty():

        # Get node, path, and position
        (myNode, myPath) = myPriorityQ.pop()
        myPosition = myNode[0]

        # Check if position in goal state
        if problem.isGoalState(myPosition):
            return myPath
           
        # Go through successors
        for successorNode in problem.getSuccessors(myPosition):

            # Get successor position and full path
            successorPosition = successorNode[0]
            successorFullPath = myPath + [successorNode[1]]
            successorOldCost =  myCostDict[successorPosition] if successorPosition in myCostDict else -1
            successorNewCost = problem.getCostOfActions(successorFullPath)

            # Check if not seen already
            if successorPosition not in myDiscoveredSet:
                # Add to set of discovered positions
                myDiscoveredSet = myDiscoveredSet.union( set([successorPosition]) )
                # Add cost
                myCostDict[successorPosition] = successorNewCost
                # Push successor onto Q with cost
                myPriorityQ.push( (successorNode, successorFullPath), successorNewCost )

            # Check if cost is lower than what is in there
            if successorPosition in myDiscoveredSet and successorOldCost > successorNewCost:
                # Change cost
                myPriorityQ.push( (successorNode, successorFullPath), successorNewCost )

    # No path found  
    return None

# //////////////////////////////////////////////////////////////////////////////

"""
A heuristic function estimates the cost from the current state to the nearest
goal in the provided SearchProblem.  This heuristic is trivial.
"""
def nullHeuristic(state, problem=None):

    return 0

# //////////////////////////////////////////////////////////////////////////////

"""Search the node that has the lowest combined cost and heuristic first."""
def aStarSearch(problem, heuristic=nullHeuristic):
    
    # Set up a priority Q, set of discovered nodes, and dictionary of costs
    myPriorityQ = util.PriorityQueue()
    myDiscoveredSet = set()
    myCostDict = {}
 
    # Get starting position, push it on priority Q (with cost 0), discovered, and add cost
    startPosition = problem.getStartState()
    startPath = []
    startNode = (startPosition, None, 1)
    myPriorityQ.push( (startNode, startPath), 0 )
    myDiscoveredSet = myDiscoveredSet.union( set([startPosition]) )
    myCostDict[startPosition] = 0 + heuristic(startPosition, problem)

    while not myPriorityQ.isEmpty():

        # Get node, path, and position
        (myNode, myPath) = myPriorityQ.pop()
        myPosition = myNode[0]

        # Check if position in goal state
        if problem.isGoalState(myPosition):
            return myPath
           
        # Go through successors
        for successorNode in problem.getSuccessors(myPosition):

            # Get successor position and full path
            successorPosition = successorNode[0]
            successorFullPath = myPath + [successorNode[1]]
            successorOldCost =  myCostDict[successorPosition] if successorPosition in myCostDict else -1
            successorNewCost = problem.getCostOfActions(successorFullPath) + heuristic(successorPosition, problem)

            # Check if not seen already
            if successorPosition not in myDiscoveredSet:
                # Add to set of discovered positions
                myDiscoveredSet = myDiscoveredSet.union( set([successorPosition]) )
                # Add cost
                myCostDict[successorPosition] = successorNewCost
                # Push successor onto Q with cost
                myPriorityQ.push( (successorNode, successorFullPath), successorNewCost )

            # Check if cost is lower than what is in there
            if successorPosition in myDiscoveredSet and successorOldCost > successorNewCost:
                # Change cost
                myPriorityQ.push( (successorNode, successorFullPath), successorNewCost )

    # No path found  
    return None

# //////////////////////////////////////////////////////////////////////////////
# //////////////////////////////////////////////////////////////////////////////

# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch

# //////////////////////////////////////////////////////////////////////////////
